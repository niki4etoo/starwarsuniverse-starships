import Starship from './Starship';

export default class StarWarsUniverse {
	constructor(){		
		this.starships = [];
		this.daysInSpace = [];
	}

	async init(){
		this._getStarshipCount();
		this._createStarships();
	}

	async _getStarshipCount(){
		//fetch the count of the starships
		
		await fetch("https://swapi.boom.dev/api/starships/")
			.then(response => response.json())
			.then(data => {
				return data.count;
			});
		
		
	}
	
	async _createStarships(){
		
		//Fetch individual starship
		var pageAvailable = true;
		var i = 1;
		
		while(pageAvailable){
			
			await fetch(`https://swapi.boom.dev/api/starships/?page=${i}?format=json`)
				.then(response => response.json())
				.then(data => {
					if(data.next == null)
						pageAvailable = false;
						
					for(var prop of data.results){
						//Checking if the data is valid, so every consumables and passengers are defined
						if(this._validateData(prop.consumables, prop.passengers)){
							
							//Init Starship
							var starship = new Starship();
							
							//Parsing consumable property
							
							var consumableNumber = parseInt(prop.consumables);
							let years = prop.consumables.endsWith("year") || prop.consumables.endsWith("years");
							let months = prop.consumables.endsWith("month") || prop.consumables.endsWith("months");
							let weeks = prop.consumables.endsWith("week") || prop.consumables.endsWith("weeks");
							(years ? consumableNumber *= 365 : (months ? consumableNumber *= 30 : (weeks ? consumableNumber *= 7 : consumableNumber)));
							
							//Parsing passengers property
							let passengers = prop.passengers.replace(",", ""); // removing comma in the passengers number
							var passengersNumber = parseInt(passengers);
							
							starship.name = prop.name;
							starship._consumables = consumableNumber;
							starship._passengers = passengersNumber;
							this.daysInSpace.push(starship.maxDaysInSpace);
							this.starships.push(starship);
						}
					}
					
					i++;
				});			
		}
		console.log(this.theBestStarship);
	}
	
	_validateData(consumables, passengers){
		if(consumables != null && consumables != "unknown" && consumables != undefined && 
		   passengers != 0  && passengers != null && passengers != undefined && passengers != "n/a" && passengers != "unknown")
			return true;
			
		return false;
	}
	
	get theBestStarship(){
		var max = Math.max.apply(null, this.daysInSpace);
		var index = this.daysInSpace.indexOf(max);
		return this.starships[index];		
	}

}
